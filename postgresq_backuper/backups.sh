#!/bin/bash

source /pgenv.sh

#echo "Running with these environment options" >> /var/log/cron.log
#set | grep PG >> /var/log/cron.log

MYDATE=`date +%d-%B-%Y`
MONTH=$(date +%B)
YEAR=$(date +%Y)
MYBASEDIR=/backups
MYBACKUPDIR=${MYBASEDIR}/${YEAR}/${MONTH}
rm -r ${MYBACKUPDIR}
mkdir -p ${MYBACKUPDIR}
cd ${MYBACKUPDIR}

echo "Backup running to $MYBACKUPDIR" >> /var/log/cron.log
echo "Pass ->  ${PGPASSWORD}"  >> /var/log/cron.log
echo "Host ->  ${PGHOST}" >> /var/log/cron.log
echo "PORT ->  ${PGPORT}" >> /var/log/cron.log
echo "USER ->  ${PGUSER}" >> /var/log/cron.log
echo "DATABASE ->  ${PGDATABASE}" >> /var/log/cron.log

#
# Loop through each pg database backing it up
#
PGPASSWORD=${PGPASSWORD} pg_dump -h ${PGHOST} -p ${PGPORT} -U ${PGUSER}  -F c -b -v -f  ${MYBACKUPDIR}/${DUMPPREFIX}-${MYDATE}.dump ${PGDATABASE}